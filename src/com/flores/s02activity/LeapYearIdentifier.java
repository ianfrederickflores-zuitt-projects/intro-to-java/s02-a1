package com.flores.s02activity;

import java.util.Scanner;

public class LeapYearIdentifier {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the year: \n");
        int year = Integer.parseInt(input.nextLine());

        if ((year % 4) == 0){
            System.out.println("The year is a leap year.");
        }

        else {
            System.out.println("The year is not a leap year.");
        }
    }
}
